# Кеширование даннных с базы postgres в redis
### Проблемма
Сделав профайлинг моей программы, я выяснил что большую часть
времени рантайма занимают запросы в базу данных postgres. Что бы повысить
пропускную способность программы, было принято решение сделать кешинг определённых данных
для postgres.

<i>Профайлинг в среде разработки WebStorm:</i>
![image](img/1.png)

Наша бд архитектура не позволяла вытянуть все данные за один запрос, по-этому для правильной сериализации данных
нам нужно делать около 4-5 запросов, что создает задержку в рантайме.

#### Тест при помощи ApacheBench:</i>
##### Конфигурация:
```
ab -p start-to-bot.txt -T application/json -c 10 -n 2000 https://bots.cloud2y.com/handler/12091*****************************
```
##### Результат:
```
Concurrency Level:      10
Time taken for tests:   18.808 seconds
Complete requests:      2000
Failed requests:        0
Non-2xx responses:      2000
Total transferred:      550000 bytes
Total body sent:        1094000
HTML transferred:       226000 bytes
Requests per second:    106.34 [#/sec] (mean)
Time per request:       94.042 [ms] (mean)
Time per request:       9.404 [ms] (mean, across all concurrent requests)
Transfer rate:          28.56 [Kbytes/sec] received
                        56.80 kb/s sent
                        85.36 kb/s total

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:       19   28   3.3     28      45
Processing:    26   60  40.2     49    1070
Waiting:       25   53  36.2     43    1067
Total:         51   89  40.6     78    1093

Percentage of the requests served within a certain time (ms)
  50%     78
  66%     88
  75%     97
  80%    107
  90%    130
  95%    150
  98%    171
  99%    298
 100%   1093 (longest request)
```
### Решение
Что бы решить проблему мы создали сервис, который с периодом в 5 сек вытягивает все
нужные нам данные преобразовывает в понятный `JSON` формат и кладёт в базу данных redis.
Теперь нашему `handler`-сервису не нужно будет делать много запросов, так как данные можно вытянуть
1 запросом и они уже будут преобразованы в нужный нам интерфейс.
##### Результат при помощи ApacheBench
```
Concurrency Level:      10
Time taken for tests:   5.890 seconds
Complete requests:      2000
Failed requests:        0
Non-2xx responses:      2000
Total transferred:      528000 bytes
Total body sent:        1094000
HTML transferred:       204000 bytes
Requests per second:    339.54 [#/sec] (mean)
Time per request:       88.356 [ms] (mean)
Time per request:       2.945 [ms] (mean, across all concurrent requests)
Transfer rate:          87.54 [Kbytes/sec] received
                        181.37 kb/s sent
                        268.91 kb/s total

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:       18   35   3.4     35      50
Processing:    35   52   6.3     51     101
Waiting:       35   50   6.0     49     101
Total:         66   87   7.2     86     136

Percentage of the requests served within a certain time (ms)
  50%     86
  66%     88
  75%     90
  80%     91
  90%     94
  95%     99
  98%    108
  99%    113
 100%    136 (longest request)
```
Как видим при помощи кеширования, мы смогли поднять пропускную способность
со 100 запросов в секунду, до 350.
