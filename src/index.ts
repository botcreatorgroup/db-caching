import 'dotenv/config';

import { Updater } from './update';

const u = new Updater();
u.start();
