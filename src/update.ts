import { pool, redisClient } from './db';
import { delay } from './utils';
import { BotObject } from './typings/redis';
import { Models } from './models';

export class Updater {
  private async getFreshBotList(): Promise<Array<string>> {

    const result = await pool.query(`select token from telegram.bots`);
    return result.rows.map(row => row.token);
  }

  public async start() {
    try {
      const botList = await this.getFreshBotList();
      for (const token of botList) {
        const bot = await Models.getBot(token);

        const botObj: BotObject = {
          bot,
          schema: undefined,
          tabs: undefined
        };

        const activeSchemaId = await Models.getActiveSchemaId(bot.id);
        if (activeSchemaId) {
          // Build schema
          botObj.schema = await Models.getSchema(activeSchemaId);
          // Build tab
          const tabs = await Models.getTabs(botObj.schema.id);
          if (tabs) {
            botObj.tabs = tabs;
            await Promise.all(
              tabs.map(async tab => {
                tab.components = await Models.getComponents(tab.id);
                return tab;
              }));
            // Build components
            for (const tab of tabs) {
              if (tab.components) {
                for (const component of tab.components) {
                  const index = tab.components.indexOf(component);
                  if (component.type === 'text') {
                    tab.components[index] = { ...component, ...await Models.getTextComponent(component.id) };
                  }
                  if (component.type === 'link') {
                    tab.components[index] = { ...component, ...await Models.getLinkComponent(component.id) };
                  }
                }
              }
            }
          }
        }
        const json = JSON.stringify(botObj);
        // eslint-disable-next-line no-loop-func
        await redisClient.set(token, json, (err, reply) => {
          if (reply) {
            console.log(json);
          }
        });
      }

      await delay(5000);
      await this.start();
    } catch (error) {
      console.error(error);
    }
  }
}
