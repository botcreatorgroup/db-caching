import { Pool } from 'pg';
import { ClientOpts, createClient } from 'redis';

const pdbConf = {
  DB_USER: process.env.DB_USER,
  DB_PASSWORD: process.env.DB_PASSWORD,
  DB_HOST: process.env.DB_HOST,
  DB_NAME: process.env.DB_NAME

};
export const pool = new Pool({
  connectionString: `postgres://${pdbConf.DB_USER}:${pdbConf.DB_PASSWORD}@${pdbConf.DB_HOST}/${pdbConf.DB_NAME}`,
  max: 30
});

const rdbConf: ClientOpts = {
  host: process.env.REDIS_HOST,
  port: parseInt(<string>process.env.REDIS_PORT, 10)
};

export const redisClient = createClient(rdbConf);
redisClient.on('error', err => {
  console.log('REDIS ERROR:', err);
});
