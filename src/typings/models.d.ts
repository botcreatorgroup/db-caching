

export interface BotModel {
  id: number;
  title: string;
  token: string;
  isRunning: boolean;
  created: Date;
}

export interface SchemaModel {
  id: number;
  title: string;
  created: Date;
}

export interface TabModel {
  id: number;
  title: string;
  created: Date;
  components?: Array<ComponentModel<any>>;
}

export type ComponentType = 'text' | 'image' | 'document' | 'link';

export interface ComponentModel<T extends ComponentType> {
  id: number;
  type: T;
  created: Date;
}

//  Also add Keyboard and (?)Button Model
export interface TextComponentModel extends ComponentModel<'text'> {
  content: string;
}

export interface ImageComponentModel extends ComponentModel<'image'> {
  raw: ArrayBuffer;
  size: number;
  name: string;
}

export interface DocumentComponentModel extends ComponentModel<'document'> {
  raw: ArrayBuffer;
  size: number;
  name: string;
}

export interface LinkComponentModel extends ComponentModel<'link'> {
  linkToNextTab: number;
}
