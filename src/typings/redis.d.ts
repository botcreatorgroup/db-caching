import { BotModel, SchemaModel, TabModel } from './models';

export interface BotObject {
    bot: BotModel,
    schema: SchemaModel | undefined,
    tabs: Array<TabModel> | undefined,
}
