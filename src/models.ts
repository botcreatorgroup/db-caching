import { BotModel, ComponentModel, ComponentType, SchemaModel, TabModel } from './typings/models';
import { pool } from './db';
import { LinkComponentModel, TextComponentModel } from './typings/models';

export class Models {
  public static async getBot(token: string): Promise<BotModel> {
    await pool.query('set search_path to telegram');

    const { rows } = await pool.query(
      `select id, title, token, is_running as "isRunning", created
       from bots
       where token = $1`, [token]);
    return rows[0];
  }

  public static async getActiveSchemaId(botId: number) {
    await pool.query('set search_path to telegram');
    try {
      const result = await pool.query(
        `select active_schema as "activeSchema" from bots
         where bots.id = ${botId}`
      );
      return result.rows[0].activeSchema;
    } catch (e) {
      return undefined;
    }

  }

  public static async getSchema(schemaId: number): Promise<SchemaModel> {
    await pool.query('set search_path to telegram');
    const result = await pool.query(
      `select id, title, created
       from schema
       where id = $1`,
      [schemaId]
    );
    return result.rows[0];
  }

  public static async getTabs(schemaId: number): Promise<Array<TabModel> | undefined> {
    await pool.query('set search_path to telegram');
    try {
      const { rows } = await pool.query(
        `select tab.id, tab.title, tab.created
       from tab
       inner join schema s on s.id = tab.schema_id
       where s.id = $1
       order by created`,
        [schemaId]
      );
      return rows;
    } catch (e) {
      return undefined;
    }

  }

  public static async getComponents(
    tabId: number
  ): Promise<Array<ComponentModel<ComponentType>> | undefined> {
    await pool.query('set search_path to telegram');
    try {
      const { rows } = await pool.query<ComponentModel<any>>(
        `select component_id as "id", component_type as type
       from tab_components
       where tab_id = $1`,
        [tabId]
      );
      return rows;
    } catch (e) {
      return undefined;
    }
  }


  public static async getLinkComponent(id: number): Promise<LinkComponentModel> {
    await pool.query('set search_path to telegram');

    const result = await pool.query(
      `select id, 'link' as type, next_tab_id as "linkToNextTab" from links
       where id = $1`,
      [id]
    );
    return result.rows[0];
  }

  public static async getTextComponent(id: number): Promise<TextComponentModel> {
    await pool.query('set search_path to telegram');

    const result = await pool.query(
      `select id, 'text' as type, content from texts
       where id = $1`,
      [id]
    );
    return result.rows[0];
  }
}
